#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>

#include "server.h"
#include "decoration.h"

static void handle_new_toplevel_decoration(struct wl_listener *listener, void *data);
static struct nc_decoration *nc_decoration_create(
        struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_toplevel_decoration,
        struct nc_decoration_manager *decoration_manager);
static void handle_request_mode(struct wl_listener *listener, void *data);

struct nc_decoration_manager *nc_decoration_manager_create(struct nc_server *server) {
    struct nc_decoration_manager *decoration_manager =
        calloc(1, sizeof(struct nc_decoration_manager));
    decoration_manager->server = server;
    decoration_manager->wlr_xdg_decoration_manager =
        wlr_xdg_decoration_manager_v1_create(server->wl_display);

    wl_list_init(&decoration_manager->decorations);

    decoration_manager->listen_new_toplevel_decoration.notify = handle_new_toplevel_decoration;
    wl_signal_add(&decoration_manager->wlr_xdg_decoration_manager->events.new_toplevel_decoration,
            &decoration_manager->listen_new_toplevel_decoration);

    return decoration_manager;
}

static void handle_new_toplevel_decoration(struct wl_listener *listener, void *data) {
    struct nc_decoration_manager *decoration_manager = 
        wl_container_of(listener, decoration_manager, listen_new_toplevel_decoration);
    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_decoration = data;

    struct nc_decoration *decoration = nc_decoration_create(wlr_xdg_decoration,
            decoration_manager);
    wl_list_insert(&decoration_manager->decorations, &decoration->link);
}

static struct nc_decoration *nc_decoration_create(
        struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_toplevel_decoration,
        struct nc_decoration_manager *decoration_manager) {
    struct nc_decoration *decoration = calloc(1, sizeof(struct nc_decoration));
    decoration->decoration_manager = decoration_manager;
    decoration->wlr_xdg_toplevel_decoration = wlr_xdg_toplevel_decoration;

    decoration->listen_request_mode.notify = handle_request_mode;
    wl_signal_add(&decoration->wlr_xdg_toplevel_decoration->events.request_mode,
            &decoration->listen_request_mode);

    return decoration;
}

static void handle_request_mode(struct wl_listener *listener, void *data) {
    struct nc_decoration *decoration = wl_container_of(listener, decoration, listen_request_mode);

    wlr_xdg_toplevel_decoration_v1_set_mode(decoration->wlr_xdg_toplevel_decoration,
            WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_CLIENT_SIDE);
}
