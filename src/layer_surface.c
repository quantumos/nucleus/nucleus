#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_output.h>

#include "output.h"
#include "server.h"
#include "layer_surface.h"

static struct nc_layer_surface *layer_surface_create(
        struct wlr_layer_surface_v1 *wlr_layer_surface);
static void arrange_layers(struct nc_output *output);
static void arrange_layer(struct nc_output *output, struct wl_list *list,
        struct wlr_box *usable_area, bool exclusive);
static void apply_exclusive(struct wlr_box *usable_area,
        uint32_t anchor, int32_t exclusive,
        int32_t margin_top, int32_t margin_left,
        int32_t margin_bottom, int32_t margin_right) {
    if (exclusive <= 0) {
        return;
    }
    struct {
        uint32_t anchors;
        int *positive_axis;
        int *negative_axis;
        int margin;
    } edges[] = {
        {
            .anchors =
                ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP,
            .positive_axis = &usable_area->y,
            .negative_axis = &usable_area->height,
            .margin = margin_top,
        },
        {
            .anchors =
                ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = NULL,
            .negative_axis = &usable_area->height,
            .margin = margin_top,
        },
        {
            .anchors =
                ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = &usable_area->x,
            .negative_axis = &usable_area->width,
            .margin = margin_left,
        },
        {
            .anchors =
                ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM,
            .positive_axis = NULL,
            .negative_axis = &usable_area->width,
            .margin = margin_right,
        },
    };
    for (size_t i = 0;i < sizeof(edges) / sizeof(edges[0]);i++) {
        if ((anchor & edges[i].anchors) == edges[i].anchors && exclusive + edges[i].margin > 0) {
            if (edges[i].positive_axis) {
                *edges[i].positive_axis += exclusive + edges[i].margin;
            }
            if (edges[i].negative_axis) {
                *edges[i].negative_axis -= exclusive + edges[i].margin;
            }
        }
    }
}

static void handle_map(struct wl_listener *listener, void *data);
static void handle_unmap(struct wl_listener *listener, void *data);
static void handle_destroy(struct wl_listener *listener, void *data);
static void handle_commit(struct wl_listener *listener, void *data);

void handle_new_layer_surface(struct wl_listener *listener, void *data) {
    struct wlr_layer_surface_v1 *wlr_layer_surface = data;
    wlr_log(WLR_INFO, "New layer surface: namespace %s layer %d anchor %d"
            "size %dx%d margin %d,%d,%d,%d",
            wlr_layer_surface->namespace,
            wlr_layer_surface->client_pending.layer,
            wlr_layer_surface->client_pending.layer,
            wlr_layer_surface->client_pending.desired_width,
            wlr_layer_surface->client_pending.desired_height,
            wlr_layer_surface->client_pending.margin.top,
            wlr_layer_surface->client_pending.margin.right,
            wlr_layer_surface->client_pending.margin.bottom,
            wlr_layer_surface->client_pending.margin.left);

    struct nc_server *server = wl_container_of(listener, server, listen_new_layer_surface);

    // if the layer surface doesn't have an output assigned to it, assign the first output
    // or close the layer if there aren't any outputs
    if (!wlr_layer_surface->output) {
        struct nc_output *output = NULL;

        if (!wl_list_empty(&server->root->outputs)) {
            output = wl_container_of(server->root->outputs.next, output, link);
            wlr_log(WLR_INFO, "Layer surface didn't have an output, assigning output: %s",
                    output->wlr_output->name);
            wlr_layer_surface->output = output->wlr_output;
        } else {
            wlr_layer_surface_v1_close(wlr_layer_surface);
            return;
        }
    }

    struct nc_layer_surface *layer_surface = layer_surface_create(wlr_layer_surface);
    struct nc_output *output = layer_surface->output;

    // temporarily set the layer's current state to client_pending
    // so that we can easily arrange it
    struct wlr_layer_surface_v1_state old_state = wlr_layer_surface->current;
    wlr_layer_surface->current = wlr_layer_surface->client_pending;
    wl_list_insert(&output->layers[wlr_layer_surface->client_pending.layer],
            &layer_surface->link);
    arrange_layers(output);
    wl_list_remove(&layer_surface->link);
    wlr_layer_surface->current = old_state;
}

static struct nc_layer_surface *layer_surface_create(
        struct wlr_layer_surface_v1 *wlr_layer_surface) {
    struct nc_layer_surface *layer_surface = calloc(1, sizeof(struct nc_layer_surface));

    layer_surface->wlr_layer_surface = wlr_layer_surface;
    wlr_layer_surface->data = layer_surface;
    layer_surface->output = wlr_layer_surface->output->data;

    layer_surface->layer = wlr_layer_surface->client_pending.layer;

    layer_surface->listen_destroy.notify = handle_destroy;
    wl_signal_add(&wlr_layer_surface->events.destroy, &layer_surface->listen_destroy);
    layer_surface->listen_map.notify = handle_map;
    wl_signal_add(&wlr_layer_surface->events.map, &layer_surface->listen_map);
    layer_surface->listen_unmap.notify = handle_unmap;
    wl_signal_add(&wlr_layer_surface->events.unmap, &layer_surface->listen_unmap);

    return layer_surface;
}

static void handle_map(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Layer surface mapped");
    struct nc_layer_surface *layer_surface = wl_container_of(listener, layer_surface, listen_map);
    struct wlr_layer_surface_v1 *wlr_layer_surface = layer_surface->wlr_layer_surface;


    layer_surface->listen_commit.notify = handle_commit;
    wl_signal_add(&wlr_layer_surface->surface->events.commit, &layer_surface->listen_commit);

    wlr_surface_send_enter(wlr_layer_surface->surface, wlr_layer_surface->output);
    struct nc_server *server = layer_surface->output->server;
    wlr_seat_pointer_notify_enter(server->seat->wlr_seat, wlr_layer_surface->surface,
            server->seat->cursor->x, server->seat->cursor->y);

    int layer_idx = layer_surface->layer;
    wl_list_insert(&layer_surface->output->layers[layer_idx], &layer_surface->link);
}

static void handle_unmap(struct wl_listener *listener, void *data) {
    struct nc_layer_surface *layer_surface = wl_container_of(listener, layer_surface, listen_unmap);

    if (layer_surface->wlr_layer_surface->mapped) {
        wl_list_remove(&layer_surface->listen_commit.link);
    }

    wl_list_remove(&layer_surface->link);

    // todo clear focus
    struct nc_seat *seat = layer_surface->output->server->seat;
    wlr_seat_pointer_clear_focus(seat->wlr_seat);
    wlr_surface_send_leave(layer_surface->wlr_layer_surface->surface,
            layer_surface->output->wlr_output);
    nc_seat_handle_layer_unmap(seat, layer_surface);
}

static void handle_commit(struct wl_listener *listener, void *data) {
    struct nc_layer_surface *layer_surface = wl_container_of(listener, layer_surface, listen_commit);
    struct wlr_layer_surface_v1 *wlr_layer_surface = layer_surface->wlr_layer_surface;

    if (!layer_surface->output) {
        wlr_log(WLR_ERROR, "Layer surface commited with null output");
        return;
    }

    if (layer_surface->layer != wlr_layer_surface->current.layer) {
        wl_list_remove(&layer_surface->link);

        layer_surface->layer = wlr_layer_surface->current.layer;
        wl_list_insert(&layer_surface->output->layers[layer_surface->layer],
                &layer_surface->link);
    }
}

static void handle_destroy(struct wl_listener *listener, void *data) {
    struct nc_layer_surface *layer_surface = wl_container_of(listener, layer_surface, listen_destroy);

    wlr_log(WLR_INFO, "Layer surface destroyed");

    wl_list_remove(&layer_surface->listen_destroy.link);
    wl_list_remove(&layer_surface->listen_map.link);
    wl_list_remove(&layer_surface->listen_unmap.link);

    free(layer_surface);
}

static void arrange_layers(struct nc_output *output) {
    struct wlr_box usable_area = { 0 };
    wlr_output_effective_resolution(output->wlr_output,
            &usable_area.width, &usable_area.height);

    // arrange exclusive surfaces from top to bottom
    size_t len = sizeof(output->layers) / sizeof(output->layers[0]);
    for (size_t i = 0;i < len;i++) {
        arrange_layer(output, &output->layers[i], &usable_area, true);
    }

    for (size_t i = 0;i < len;i++) {
        arrange_layer(output, &output->layers[i], &usable_area, false);
    }

    // find topmost keyboard interactive layer, if such a layer exists
    // TODO implement focus and stuff, its a lot of code
    uint32_t layers_above_shell[] = {
         ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
         ZWLR_LAYER_SHELL_V1_LAYER_TOP,
    };
    size_t nlayers = sizeof(layers_above_shell) / sizeof(layers_above_shell[0]);
    struct nc_layer_surface *layer, *topmost = NULL;
    for (size_t i = 0;i < nlayers;i++) {
        wl_list_for_each_reverse(layer,
                &output->layers[layers_above_shell[i]], link) {
            if (layer->wlr_layer_surface->current.keyboard_interactive &&
                    layer->wlr_layer_surface->mapped) {
                topmost = layer;
                break;
            }
        }
        if (topmost != NULL) {
            break;
        }
    }

    if (topmost) {
        struct nc_seat *seat = output->server->seat;
        struct nc_focus_target target = {
            .type = NC_FOCUS_TARGET_LAYER,
            .layer_surface = layer,
        };

        nc_seat_focus_target(seat, &target);
    }
}

static void arrange_layer(struct nc_output *output, struct wl_list *list,
        struct wlr_box *usable_area, bool exclusive) {
    struct nc_layer_surface *layer_surface;
    struct wlr_box full_area = { 0 };
    wlr_output_effective_resolution(output->wlr_output,
            &full_area.width, &full_area.height);

    wl_list_for_each(layer_surface, list, link) {
        struct wlr_layer_surface_v1 *layer = layer_surface->wlr_layer_surface;
        struct wlr_layer_surface_v1_state *state = &layer->current;
        if (exclusive != (state->exclusive_zone > 0)) {
            continue;
        }
        struct wlr_box bounds;
        if (state->exclusive_zone == -1) {
            bounds = full_area;
        } else {
            bounds = *usable_area;
        }
        struct wlr_box box = {
            .width = state->desired_width,
            .height = state->desired_height,
        };

        // horizontal axis
        const uint32_t both_horiz = ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT
            | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT;
        if ((state->anchor & both_horiz) && box.width == 0) {
            box.x = bounds.x;
            box.width = bounds.width;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT)) {
            box.x = bounds.x;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT)) {
            box.x = bounds.x + (bounds.width - box.width);
        } else {
            box.x = bounds.x + ((bounds.width / 2) - (box.width / 2));
        }
        
        // vertical axis
        const uint32_t both_vert = ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP
            | ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM;
        if ((state->anchor & both_vert) && box.height == 0) {
            box.y = bounds.y;
            box.height = bounds.height;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP)) {
            box.y = bounds.y;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM)) {
            box.y = bounds.y + (bounds.height - box.height);
        } else {
            box.y = bounds.y + ((bounds.height / 2) - (box.height / 2));
        }

        // margin
        if ((state->anchor & both_horiz) == both_horiz) {
            box.x += state->margin.left;
            box.width -= state->margin.left + state->margin.right;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT)) {
            box.x += state->margin.left;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT)) {
            box.x -= state->margin.right;
        }
        if ((state->anchor & both_vert) == both_vert) {
            box.y += state->margin.top;
            box.height -= state->margin.top + state->margin.bottom;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP)) {
            box.y += state->margin.top;
        } else if ((state->anchor & ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM)) {
            box.y -= state->margin.bottom;
        }
        if (box.width < 0 || box.height < 0) {
            wlr_layer_surface_v1_close(layer);
            continue;
        }

        // apply
        layer_surface->box = box;
        apply_exclusive(usable_area, state->anchor, state->exclusive_zone,
                state->margin.top, state->margin.right,
                state->margin.bottom, state->margin.left);
        wlr_layer_surface_v1_configure(layer, box.width, box.height);
    }
}
