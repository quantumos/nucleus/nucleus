#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wlr/util/log.h>

#include "server.h"

int main(int argc, char **argv) {
    // Since wayland requires XDG_RUNTIME_DIR to be set, abort with just the
    // clear error message.
    if (!getenv("XDG_RUNTIME_DIR")) {
        fprintf(stderr,
                "XDG_RUNTIME_DIR is not set in the environment. Aborting.\n");
        exit(EXIT_FAILURE);
    }

    wlr_log_init(WLR_DEBUG, NULL);

    struct nc_server server;
    if (!nc_server_init(&server)) {
        return 1;
    }

    if (!nc_server_start(&server)) {
        return 1;
    }

    setenv("WAYLAND_DISPLAY", server.socket, true);
    setenv("GDK_BACKEND", "wayland", true);
    if (argc > 1) {
        if (argc % 2 == 0) goto usage_failure;
        for (int i = 1;i < argc;i += 2) {
            if (strcmp(argv[i], "-s")) goto usage_failure;
            if (fork() == 0) {
                execl("/bin/sh", "/bin/sh", "-c", argv[i + 1], (char *)NULL);
            }
        }
    }

    nc_server_run(&server);
    nc_server_destroy(&server);

    return 0;

usage_failure:
    printf("Incorrect usage\n");
    exit(EXIT_FAILURE);
}
