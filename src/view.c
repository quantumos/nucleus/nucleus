#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_xdg_shell.h>

#include "server.h"
#include "transaction.h"
#include "view.h"

static void handle_map(struct wl_listener *listener, void *data);
static void handle_unmap(struct wl_listener *listener, void *data);
static void handle_move(struct wl_listener *listener, void *data);
static void handle_resize(struct wl_listener *listener, void *data);
static void handle_maximize(struct wl_listener *listener, void *data);
static void handle_minimize(struct wl_listener *listener, void *data);
static void handle_commit(struct wl_listener *listener, void *data);
static void handle_destroy(struct wl_listener *listener, void *data);
static void begin_interactive(struct nc_view *view,
        enum nc_cursor_mode mode, uint32_t edges);
static bool view_size_changed(struct nc_view *view, int width, int height);
static bool view_tiled_changed(struct nc_view *view, uint32_t tiled_edges);
static bool view_maximized_changed(struct nc_view *view, bool maximized);
static void send_frame_done_iterator(struct wlr_surface *surface,
        int sx, int sy, void *data);

void handle_new_xdg_surface(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Creating new xdg surface");
    struct nc_server *server = wl_container_of(listener, server,
            listen_new_xdg_surface);
    struct wlr_xdg_surface *xdg_surface = data;
    if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
        return;
    }

    struct nc_view *view = calloc(1, sizeof(struct nc_view));
    view->server = server;
    view->xdg_surface = xdg_surface;
    view->mapped = false;
    view->current_box = (struct wlr_box) { 0 };
    // TODO better placement algorithm, we just offset so decorations and such draws correctly
    view->current_box.x = 50;
    view->current_box.y = 50;
    view->tiled_edges = 0;
    xdg_surface->data = view;

    view->listen_map.notify = handle_map;
    wl_signal_add(&xdg_surface->events.map, &view->listen_map);
    view->listen_unmap.notify = handle_unmap;
    wl_signal_add(&xdg_surface->events.unmap, &view->listen_unmap);
    view->listen_destroy.notify = handle_destroy;
    wl_signal_add(&xdg_surface->events.destroy, &view->listen_destroy);

    struct wlr_xdg_toplevel *toplevel = xdg_surface->toplevel;
    view->listen_move.notify = handle_move;
    wl_signal_add(&toplevel->events.request_move, &view->listen_move);
    view->listen_resize.notify = handle_resize;
    wl_signal_add(&toplevel->events.request_resize, &view->listen_resize);
    view->listen_maximize.notify = handle_maximize;
    wl_signal_add(&toplevel->events.request_maximize, &view->listen_maximize);
    view->listen_minimize.notify = handle_minimize;
    wl_signal_add(&toplevel->events.request_minimize, &view->listen_minimize);

    wl_list_insert(&server->root->views, &view->link);
}

static void handle_map(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_map);
    view->mapped = true;
    struct wlr_box geo_box;
    wlr_xdg_surface_get_geometry(view->xdg_surface, &geo_box);
    view->current_box.width = geo_box.width;
    view->current_box.height = geo_box.height;

    struct nc_server *server = view->server;
    nc_seat_focus_view(server->seat, view);
    wlr_seat_pointer_notify_enter(server->seat->wlr_seat, view->xdg_surface->surface,
            server->seat->cursor->x, server->seat->cursor->y);

    view->listen_commit.notify = handle_commit;
    wl_signal_add(&view->xdg_surface->surface->events.commit, &view->listen_commit);
}

static void handle_unmap(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_unmap);
    view->mapped = false;

    nc_seat_handle_view_unmap(view->server->seat, view);
}

static void handle_move(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_move);

    begin_interactive(view, NC_CURSOR_MODE_MOVE, 0);
}

static void handle_resize(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Starting resize");
    struct nc_view *view = wl_container_of(listener, view, listen_resize);
    struct wlr_xdg_toplevel_resize_event *event = data;

    begin_interactive(view, NC_CURSOR_MODE_RESIZE, event->edges);
}

static void handle_maximize(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Maximizing");
    struct nc_view *view = wl_container_of(listener, view, listen_maximize);

    struct nc_server *server = view->server;
    // TODO: potentially better logic for choosing which output
    struct wlr_output *output = wlr_output_layout_output_at(
            server->root->wlr_output_layout,
            server->seat->cursor->x,
            server->seat->cursor->y);
    struct wlr_output_layout_output *layout_output =
        wlr_output_layout_get(server->root->wlr_output_layout, output);

    struct wlr_box box;
    bool maximized;

    if (view->maximized) {
        box = view->untiled_box;
        maximized = false;
    } else {
        view->untiled_box = view->current_box;
        box = (struct wlr_box) {
            .x = layout_output->x,
            .y = layout_output->y,
            .width = output->width,
            .height = output->height,
        };

        maximized = true;
    }

    nc_transaction_clear(server->root->transaction);
    nc_view_set_box(view, box.x, box.y, box.width, box.height);
    nc_view_set_maximized(view, maximized);
    nc_transaction_start(server->root->transaction);
}

static void handle_minimize(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_minimize);

    nc_view_minimize(view);
    nc_seat_focus_view(view->server->seat, NULL);
}

static void handle_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Destroying");
    struct nc_view *view = wl_container_of(listener, view, listen_destroy);

    wl_list_remove(&view->listen_map.link);
    wl_list_remove(&view->listen_unmap.link);
    wl_list_remove(&view->listen_move.link);
    wl_list_remove(&view->listen_resize.link);
    wl_list_remove(&view->listen_maximize.link);
    wl_list_remove(&view->listen_minimize.link);
    wl_list_remove(&view->listen_destroy.link);

    wl_list_remove(&view->link);

    free(view);
}

void nc_focus_view(struct nc_view *view, struct wlr_surface *surface) {
    if (view == NULL) {
        return;
    }

    struct nc_server *server = view->server;
    wl_list_remove(&view->link);
    wl_list_insert(&server->root->views, &view->link);

    wlr_xdg_toplevel_set_activated(view->xdg_surface, true);
}

static void begin_interactive(struct nc_view *view,
        enum nc_cursor_mode mode, uint32_t edges) {
    struct nc_server *server = view->server;

    struct wlr_surface *focused_surface =
        server->seat->wlr_seat->pointer_state.focused_surface;
    if (view->xdg_surface->surface != focused_surface) {
        // client isn't focused, don't accept request
        return;
    }

    server->grab_state.view = view;
    server->seat->cursor_mode = mode;

    switch (mode) {
    case NC_CURSOR_MODE_MOVE:
        server->grab_state.grab_x = server->seat->cursor->x - view->current_box.x;
        server->grab_state.grab_y = server->seat->cursor->y - view->current_box.y;
        break;
    case NC_CURSOR_MODE_RESIZE:;
        struct wlr_box geo_box;
        wlr_xdg_surface_get_geometry(view->xdg_surface, &geo_box);

        double border_x = (view->current_box.x + geo_box.x) + ((edges & WLR_EDGE_RIGHT) ? geo_box.width : 0);
        double border_y = (view->current_box.y + geo_box.y) + ((edges & WLR_EDGE_BOTTOM) ? geo_box.height : 0);
        server->grab_state.grab_x = server->seat->cursor->x - border_x;
        server->grab_state.grab_y = server->seat->cursor->y - border_y;

        server->grab_state.box = geo_box;
        server->grab_state.box.x += view->current_box.x;
        server->grab_state.box.y += view->current_box.y;

        server->grab_state.resize_edges = edges;
        /*printf("%d %d %d %d\n", edges & WLR_EDGE_TOP, edges & WLR_EDGE_RIGHT,*/
                /*edges & WLR_EDGE_BOTTOM, edges & WLR_EDGE_LEFT);*/
        break;
    default:
        break;
    }
}

void nc_move_view(struct nc_view *view, double x, double y) {
    view->current_box.x = x;
    view->current_box.y = y;
}

void nc_view_stash_buffer(struct nc_view *view) {
    if (view->stashed_buffer) {
        nc_view_unstash_buffer(view);
    }

    if (view->xdg_surface && wlr_surface_has_buffer(view->xdg_surface->surface)) {
        wlr_buffer_lock(&view->xdg_surface->surface->buffer->base);
        view->stashed_buffer = view->xdg_surface->surface->buffer;
        view->stashed_width = view->xdg_surface->surface->current.width;
        view->stashed_height = view->xdg_surface->surface->current.height;
    }
}

void nc_view_unstash_buffer(struct nc_view *view) {
    if (view->stashed_buffer) {
        wlr_buffer_unlock(&view->stashed_buffer->base);
        view->stashed_buffer = NULL;
    }
}

static void handle_commit(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_commit);

    struct nc_transaction *transaction = view->server->root->transaction;

    // TODO: try to optimize
    struct nc_transaction_instruction *instruction, *tmp;
    wl_list_for_each_safe(instruction, tmp, &transaction->instructions, link) {
        if (instruction->target == view) {
            if (instruction->serial == view->xdg_surface->configure_serial) {
                nc_transaction_notify(transaction);
            } else {
                nc_view_send_frame_done(view);
            }
        }
    }
}

static bool view_size_changed(struct nc_view *view, int width, int height) {
    return (view->current_box.width != width) ||
        (view->current_box.height != height);
}

static bool view_tiled_changed(struct nc_view *view, uint32_t tiled_edges) {
    return (view->tiled_edges != tiled_edges);
}

static bool view_maximized_changed(struct nc_view *view, bool maximized) {
    return (view->maximized != maximized);
}

struct nc_transaction_instruction *nc_view_set_box(struct nc_view *view,
        int x, int y,
        int width, int height) {
    if (!view_size_changed(view, width, height)) {
        return NULL;
    }

    struct nc_transaction_instruction *instruction =
        calloc(1, sizeof(struct nc_transaction_instruction));
    instruction->target = view;
    instruction->type = NC_TRA_INS_SET_BOX;
    instruction->set_box_data.x = x;
    instruction->set_box_data.y = y;
    instruction->set_box_data.width = width;
    instruction->set_box_data.height = height;

    nc_transaction_add_instruction(view->server->root->transaction,
            instruction);

    return instruction;
}

struct nc_transaction_instruction *nc_view_set_tiled(struct nc_view *view,
        uint32_t tiled_edges) {
    if (!view_tiled_changed(view, tiled_edges)) {
        return NULL;
    }

    struct nc_transaction_instruction *instruction =
        calloc(1, sizeof(struct nc_transaction_instruction));
    instruction->target = view;
    instruction->type = NC_TRA_INS_SET_TILED;
    instruction->set_tiled_data = tiled_edges;

    nc_transaction_add_instruction(view->server->root->transaction,
            instruction);

    return instruction;
}

struct nc_transaction_instruction *nc_view_set_maximized(struct nc_view *view,
        bool maximized) {
    if (!view_maximized_changed(view, maximized)) {
        return NULL;
    }

    struct nc_transaction_instruction *instruction =
        calloc(1, sizeof(struct nc_transaction_instruction));
    instruction->target = view;
    instruction->type = NC_TRA_INS_SET_MAXIMIZED;
    instruction->set_maximized_data = maximized;

    nc_transaction_add_instruction(view->server->root->transaction,
            instruction);

    return instruction;
}

void nc_view_send_frame_done(struct nc_view *view) {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    wlr_surface_for_each_surface(view->xdg_surface->surface, send_frame_done_iterator, &now);
}

void nc_view_set_focused(struct nc_view *view, bool focused) {
    wlr_xdg_toplevel_set_activated(view->xdg_surface, focused);
}

void nc_view_minimize(struct nc_view *view) {
    // just remove it fro the root views list
    wl_list_remove(&view->link);
    nc_seat_focus_view(view->server->seat, NULL);
}

void nc_view_unminimize(struct nc_view *view) {
    wl_list_insert(&view->server->root->views, &view->link);
}

static void send_frame_done_iterator(struct wlr_surface *surface,
        int sx, int sy, void *data) {
    struct timespec *when = data;
    wlr_surface_send_frame_done(surface, when);
}
