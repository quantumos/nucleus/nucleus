#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/backend.h>
#include <wlr/backend/noop.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/types/wlr_export_dmabuf_v1.h>

#include "output.h"
#include "seat.h"
#include "view.h"
#include "layer_surface.h"
#include "root.h"
#include "decoration.h"
#include "server.h"

// initializes the nucleus server
bool nc_server_init(struct nc_server *server) {
    // create a wayland display object, which is the central object in both
    // compositors and clients
    server->wl_display = wl_display_create();
    server->wl_event_loop = wl_display_get_event_loop(server->wl_display);

    // wlroots backend abstracts the different libraries and backends
    // like DRM, wayland nested, X11 nested, etc
    server->backend = wlr_backend_autocreate(server->wl_display, NULL);
    server->noop_backend = wlr_noop_backend_create(server->wl_display);

    // initialize our renderer
    server->renderer = wlr_backend_get_renderer(server->backend);
    wlr_renderer_init_wl_display(server->renderer, server->wl_display);

    // place for clients to put their surfaces
    wlr_compositor_create(server->wl_display, server->renderer);
    // manages clipboard
    wlr_data_device_manager_create(server->wl_display);
    // manages external clipboards
    wlr_data_control_manager_v1_create(server->wl_display);

    // gamma control, for stuff like redshift/night mode
    wlr_gamma_control_manager_v1_create(server->wl_display);

    // screen copy
    wlr_screencopy_manager_v1_create(server->wl_display);
    // dmabuf for low latency screencapture
    wlr_export_dmabuf_manager_v1_create(server->wl_display);

    server->root = nc_root_create(server);
    wlr_xdg_output_manager_v1_create(server->wl_display, server->root->wlr_output_layout);

    server->seat = nc_seat_create(server);

    server->decoration_manager = nc_decoration_manager_create(server);
    server->xdg_shell = wlr_xdg_shell_create(server->wl_display);

    server->listen_new_xdg_surface.notify = handle_new_xdg_surface;
    wl_signal_add(&server->xdg_shell->events.new_surface, &server->listen_new_xdg_surface);

    server->listen_new_output.notify = handle_new_output;
    wl_signal_add(&server->backend->events.new_output, &server->listen_new_output);

    server->layer_shell = wlr_layer_shell_v1_create(server->wl_display);

    server->listen_new_layer_surface.notify = handle_new_layer_surface;
    wl_signal_add(&server->layer_shell->events.new_surface, &server->listen_new_layer_surface);

    // create a a socket for our wayland clients to connect to the compositor
    server->socket = wl_display_add_socket_auto(server->wl_display);
    if (!server->socket) {
        wlr_log(WLR_ERROR, "Unable to open wayland socket");
        wlr_backend_destroy(server->backend);
        return false;
    }

    return true;
}

// starts the compositor backend
bool nc_server_start(struct nc_server *server) {
    wlr_log(WLR_INFO, "Starting backend on wayland display '%s'",
            server->socket);
    // start the backend
    if (!wlr_backend_start(server->backend)) {
        wlr_log(WLR_ERROR, "Failed to start backend");
        wlr_backend_destroy(server->backend);
        return false;
    }

    return true;
}

// runs the server
void nc_server_run(struct nc_server *server) {
    wlr_log(WLR_INFO, "Running compositor on wayland display '%s'",
            server->socket);
    // runs the display's event loop (forever, blocking)
    wl_display_run(server->wl_display);
}

// destroys the server
void nc_server_destroy(struct nc_server *server) {
    // destroy any running clients
    wl_display_destroy_clients(server->wl_display);
    // and the display
    wl_display_destroy(server->wl_display);
}
