#define _POSIX_C_SOURCE 200809L
#include <wayland-server-core.h>
#include <wlr/util/log.h>

#include "server.h"
#include "layer_surface.h"
#include "surface.h"

static bool layer_surface_at(struct nc_output *output, struct wl_list *list,
        double lx, double ly, struct wlr_surface **surface, double *sx, double *sy);
static bool view_surface_at(struct nc_server *server, double lx, double ly,
        struct wlr_surface **surface, double *sx, double *sy);
static void send_frame_done_iterator(struct wlr_surface *surface,
        int sx, int sy, void *data);

bool nc_surface_at(struct nc_server *server,
        double lx, double ly, struct wlr_surface **surface,
        double *sx, double *sy) {
    struct wlr_output *wlr_output = wlr_output_layout_output_at(server->root->wlr_output_layout,
            lx, ly);
    /*wlr_log(WLR_INFO, "name: %s", wlr_output->name);*/
    if (!wlr_output) {
        return false;
    }
    struct nc_output *output = wlr_output->data;

    // TODO popups
    struct wlr_surface *wlr_surface;
    if(layer_surface_at(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY],
                lx, ly, &wlr_surface, sx, sy)) {
        *surface = wlr_surface;
        return true;
    }

    if(layer_surface_at(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP],
                lx, ly, &wlr_surface, sx, sy)) {
        *surface = wlr_surface;
        return true;
    }

    if (view_surface_at(server, lx, ly, &wlr_surface, sx, sy)) {
        *surface = wlr_surface;
        return true;
    }

    if(layer_surface_at(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM],
                lx, ly, &wlr_surface, sx, sy)) {
        *surface = wlr_surface;
        return true;
    }

    if(layer_surface_at(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND],
                lx, ly, &wlr_surface, sx, sy)) {
        *surface = wlr_surface;
        return true;
    }

    return false;
}

static bool layer_surface_at(struct nc_output *output, struct wl_list *list,
        double lx, double ly, struct wlr_surface **surface, double *sx, double *sy) {
    struct nc_layer_surface *layer_surface;
    wl_list_for_each(layer_surface, list, link) {
        double _sx, _sy;
        struct wlr_surface *_surface = NULL;
        _surface = wlr_layer_surface_v1_surface_at(
                layer_surface->wlr_layer_surface,
                lx - layer_surface->box.x,
                ly - layer_surface->box.y,
                &_sx, &_sy);

        if (_surface != NULL) {
            *sx = _sx;
            *sy = _sy;
            *surface = _surface;
            return true;
        }
    }

    return false;
}

static bool view_surface_at(struct nc_server *server, double lx ,double ly,
        struct wlr_surface **surface, double *sx, double *sy) {
    struct nc_view *view;
    wl_list_for_each(view, &server->root->views, link) {
        double _sx, _sy;
        struct wlr_surface *_surface = NULL;
        _surface = wlr_xdg_surface_surface_at(
                view->xdg_surface,
                lx - view->current_box.x,
                ly - view->current_box.y,
                &_sx, &_sy);

        if (_surface != NULL) {
            *sx = _sx;
            *sy = _sy;
            *surface = _surface;
            return true;
        }
    }

    return false;
}

void nc_surface_send_frame_done(struct wlr_surface *surface) {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    wlr_surface_for_each_surface(surface, send_frame_done_iterator, &now);
}

static void send_frame_done_iterator(struct wlr_surface *surface,
        int sx, int sy, void *data) {
    struct timespec *when = data;
    wlr_surface_send_frame_done(surface, when);
}
