#define _POSIX_C_SOURCE 200809L
#include <wlr/util/log.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_layer_shell_v1.h>

#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "server.h"
#include "output.h"
#include "view.h"
#include "layer_surface.h"
#include "surface.h"
#include "render.h"

static void render_surface(struct wlr_surface *surface,
        int sx, int sy, void *data);
static void render_layer(struct nc_output *output,
        struct wl_list *list, struct timespec *now);
static void scale_box(struct wlr_box *box, float scale);

void nc_render_frame(struct nc_output *output) {
    struct wlr_renderer *renderer = output->server->renderer;

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    // wlr_output_attach_render makes the OpenGL context current.
	if (!wlr_output_attach_render(output->wlr_output, NULL)) {
		return;
	}
	// The "effective" resolution can change if you rotate your outputs.
	int width, height;
	wlr_output_effective_resolution(output->wlr_output, &width, &height);

	// Begin the renderer (calls glViewport and some other GL sanity checks)
	wlr_renderer_begin(renderer, width, height);

    // clear the screen
    float color[4] = {0.3, 0.3, 0.3, 1.0}; // RGBA
    wlr_renderer_clear(renderer, color);

    render_layer(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND], &now);
    render_layer(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM], &now);

    struct nc_view *view;
    wl_list_for_each_reverse(view, &output->server->root->views, link) {
        if (!view->mapped) {
            // don't render unmapped or minimized views
            continue;
        }

        if (view->stashed_buffer) {
            double ox = 0, oy = 0;
            wlr_output_layout_output_coords(
                    output->server->root->wlr_output_layout, output->wlr_output, &ox, &oy);
            
            struct wlr_box box = {
                .x = ox + view->current_box.x,
                .y = oy + view->current_box.y,
                .width = view->stashed_width,
                .height = view->stashed_height
            };
            scale_box(&box, output->wlr_output->scale);

            float matrix[9];
            wlr_matrix_project_box(matrix, &box,
                    WL_OUTPUT_TRANSFORM_NORMAL, 0.0,
                    output->wlr_output->transform_matrix);

            wlr_render_texture_with_matrix(renderer, view->stashed_buffer->texture,
                    matrix, 1.0);
        } else {
            struct nc_surface_render_data rdata = {
                .output = output,
                .output_x = view->current_box.x,
                .output_y = view->current_box.y,
                .renderer = renderer,
                .when = &now,
                .output_local = false,
            };
            wlr_xdg_surface_for_each_surface(view->xdg_surface,
                    render_surface, &rdata);
        }
    }

    render_layer(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_TOP], &now);
    render_layer(output, &output->layers[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY], &now);

    // render software cursors (only runs if hardware cursor support isn't available)
    wlr_output_render_software_cursors(output->wlr_output, NULL);

    // end and commit frame
    wlr_renderer_end(renderer);
    wlr_output_commit(output->wlr_output);
}

static void render_surface(struct wlr_surface *surface,
        int sx, int sy, void *data) {
    struct nc_surface_render_data *rdata = data;
    struct nc_output *output = rdata->output;

    // obtain texture to render from surface
    struct wlr_texture *texture = wlr_surface_get_texture(surface);
    if (texture == NULL) {
        return;
    }

    // map global position in output layout to output-local position
    double ox = 0, oy = 0;
    if (!rdata->output_local) {
        wlr_output_layout_output_coords(
                output->server->root->wlr_output_layout, output->wlr_output, &ox, &oy);
    }
    ox += rdata->output_x + sx;
    oy += rdata->output_y + sy;

    struct wlr_box box = {
        .x = ox,
        .y = oy,
        .width = surface->current.width,
        .height = surface->current.height,
    };
    scale_box(&box, output->wlr_output->scale);

    // map out & render
    float matrix[9];
    enum wl_output_transform transform =
        wlr_output_transform_invert(surface->current.transform);
    wlr_matrix_project_box(matrix, &box, transform, 0,
            output->wlr_output->transform_matrix);

    wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1.0);

    nc_surface_send_frame_done(surface);
}

static void render_layer(struct nc_output *output,
        struct wl_list *list, struct timespec *now) {
    struct nc_layer_surface *layer_surface;
    wl_list_for_each(layer_surface, list, link) {
        struct nc_surface_render_data rdata = {
            .output = output,
            .output_x = layer_surface->box.x,
            .output_y = layer_surface->box.y,
            .renderer = output->server->renderer,
            .when = now,
            .output_local = true,
        };

        wlr_layer_surface_v1_for_each_surface(layer_surface->wlr_layer_surface,
                render_surface, &rdata);
    }
}

static void scale_box(struct wlr_box *box, float scale) {
    box->x *= scale;
    box->y *= scale;
    box->width *= scale;
    box->height *= scale;
}
