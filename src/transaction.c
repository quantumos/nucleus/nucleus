#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>

#include "root.h"
#include "server.h"
#include "transaction.h"

static int handle_timeout(void *data) {
    struct nc_transaction *transaction = data;

    wlr_log(WLR_ERROR, "Transaction timed out. Some imperfect frames may be drawn.");
    nc_transaction_commit(transaction);
    
    return 0;
}

struct nc_transaction *nc_transaction_create(struct nc_root *root) {
    struct nc_transaction *transaction = calloc(1, sizeof(struct nc_transaction));
    transaction->root = root;
    transaction->timer = wl_event_loop_add_timer(root->server->wl_event_loop,
            handle_timeout, transaction);
    wl_list_init(&transaction->instructions);
    transaction->num_pending = 0;

    return transaction;
}

void nc_transaction_add_instruction(struct nc_transaction *transaction,
        struct nc_transaction_instruction *instruction) {
    wl_list_insert(&transaction->instructions, &instruction->link);
    transaction->num_pending++;
}

void nc_transaction_clear(struct nc_transaction *transaction) {
    transaction->num_pending = 0;

    struct nc_transaction_instruction *instruction, *tmp;
    wl_list_for_each_safe(instruction, tmp, &transaction->instructions, link) {
        wl_list_remove(&instruction->link);
        free(instruction);
    }
}

void nc_transaction_start(struct nc_transaction *transaction) {
    if (transaction == NULL) {
        return;
    }

    struct nc_transaction_instruction *instruction;
    wl_list_for_each(instruction, &transaction->instructions, link) {
        switch (instruction->type) {
        case NC_TRA_INS_SET_BOX:
            instruction->serial = wlr_xdg_toplevel_set_size(instruction->target->xdg_surface,
                    instruction->set_box_data.width, instruction->set_box_data.height);
            break;
        case NC_TRA_INS_SET_TILED:
            instruction->serial = wlr_xdg_toplevel_set_tiled(instruction->target->xdg_surface,
                    instruction->set_tiled_data);
            break;
        case NC_TRA_INS_SET_MAXIMIZED:
            instruction->serial = wlr_xdg_toplevel_set_maximized(instruction->target->xdg_surface,
                    instruction->set_maximized_data);
            break;
        }

        nc_view_send_frame_done(instruction->target);
        nc_view_stash_buffer(instruction->target);
    }

    if (transaction->num_pending > 0) {
        /*wlr_log(WLR_INFO, "Starting transaction with %d pending configures",*/
                /*transaction->num_pending);*/

        // TODO: handle error
        wl_event_source_timer_update(transaction->timer, 200);
    }
}

void nc_transaction_commit(struct nc_transaction *transaction) {
    transaction->num_pending = 0;

    struct nc_transaction_instruction *instruction, *tmp;
    wl_list_for_each_safe(instruction, tmp, &transaction->instructions, link) {
        switch (instruction->type) {
        case NC_TRA_INS_SET_BOX:
            instruction->target->current_box.x = instruction->set_box_data.x;
            instruction->target->current_box.y = instruction->set_box_data.y;
            instruction->target->current_box.width = instruction->set_box_data.width;
            instruction->target->current_box.height = instruction->set_box_data.height;
            break;
        case NC_TRA_INS_SET_TILED:
            instruction->target->tiled_edges = instruction->set_tiled_data;
            break;
        case NC_TRA_INS_SET_MAXIMIZED:
            instruction->target->maximized = instruction->set_maximized_data;
            break;
        }

        nc_view_unstash_buffer(instruction->target);
        wl_list_remove(&instruction->link);
        free(instruction);
    }
}

void nc_transaction_notify(struct nc_transaction *transaction) {
    transaction->num_pending--;

    if (transaction->num_pending == 0) {
        // TODO: handle failure
        wl_event_source_timer_update(transaction->timer, 0);
        nc_transaction_commit(transaction);
    }
}
