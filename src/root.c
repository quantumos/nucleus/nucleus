#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/backend/noop.h>
#include <wlr/types/wlr_output_layout.h>

#include "server.h"
#include "output.h"
#include "view.h"
#include "transaction.h"
#include "root.h"

struct nc_root *nc_root_create(struct nc_server *server) {
    struct nc_root *root = calloc(1, sizeof(struct nc_root));
    root->server = server;

    root->wlr_output_layout = wlr_output_layout_create();
    wl_list_init(&root->outputs);
    wl_list_init(&root->views);
    root->transaction = nc_transaction_create(root);

    struct wlr_output *noop_wlr_output = wlr_noop_add_output(root->server->noop_backend);
    root->noop_output = nc_output_create(noop_wlr_output);

    return root;
}

void nc_root_destroy(struct nc_root *root) {
    struct nc_output *output, *tmp;
    wl_list_for_each_safe(output, tmp, &root->outputs, link) {
        nc_output_destroy(output);
    }

    wlr_output_layout_destroy(root->wlr_output_layout);
}
