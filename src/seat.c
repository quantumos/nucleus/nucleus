#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/backend.h>
#include <wlr/backend/multi.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_keyboard_group.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_data_device.h>
#include <xkbcommon/xkbcommon.h>

#include "view.h"
#include "surface.h"
#include "root.h"
#include "transaction.h"
#include "output.h"
#include "layer_surface.h"
#include "seat.h"

#define DEFAULT_SEAT_NAME "seat0"
#define XCURSOR_SIZE 24
#define DEFAULT_XCURSOR "left_ptr"

static void handle_new_input(struct wl_listener *listener, void *data);
static void handle_destroy(struct wl_listener *listener, void *data);
static void handle_new_keyboard(struct nc_seat *seat, struct wlr_input_device *device);
static void handle_new_pointer(struct nc_seat *seat, struct wlr_input_device *device);
static void handle_pointer_destroy(struct wl_listener *listener, void *data);
static void handle_request_set_cursor(struct wl_listener *listener, void *data);
static void handle_cursor_motion(struct wl_listener *listener, void *data);
static void handle_cursor_motion_absolute(struct wl_listener *listener, void *data);
static void handle_cursor_button(struct wl_listener *listener, void *data);
static void handle_cursor_axis(struct wl_listener *listener, void *data);
static void handle_cursor_frame(struct wl_listener *listener, void *data);
static void update_capabilities(struct nc_seat *seat);
static void process_cursor_motion(struct nc_seat *seat, uint32_t time);
static void process_cursor_move(struct nc_seat *seat, uint32_t time);
static void process_cursor_resize(struct nc_seat *seat, uint32_t time);
static void keyboard_group_add(struct wlr_input_device *device, struct nc_seat *seat);
static void handle_keyboard_group_key(struct wl_listener *listener, void *data);
static void handle_keyboard_group_modifiers(struct wl_listener *listener, void *data);
static void handle_key_event(struct wlr_input_device *device, struct nc_seat *seat, void *data);
static void handle_modifier_event(struct wlr_input_device *device, struct nc_seat *seat);
static bool handle_keybind(struct nc_seat *seat, xkb_keysym_t keysym, uint32_t modifiers);
static bool handle_builtin_keybind(struct nc_seat *seat, xkb_keysym_t keysym);
static void handle_request_set_selection(struct wl_listener *listener, void *data);

struct nc_seat *nc_seat_create(struct nc_server *server) {
    wlr_log(WLR_INFO, "Creating seat");
    struct nc_seat *seat = calloc(1, sizeof(struct nc_seat));

    seat->wlr_seat = wlr_seat_create(server->wl_display, DEFAULT_SEAT_NAME);
    if (!seat->wlr_seat) {
        wlr_log(WLR_ERROR, "Cannot allocate seat '%s'", DEFAULT_SEAT_NAME);
        free(seat);
        return NULL;
    }

    struct wlr_backend *backend = server->backend;

    seat->server = server;
    seat->listen_destroy.notify = handle_destroy;
    wl_signal_add(&seat->wlr_seat->events.destroy, &seat->listen_destroy);

    seat->listen_new_input.notify = handle_new_input;
    wl_signal_add(&backend->events.new_input, &seat->listen_new_input);

    wl_list_init(&seat->pointers);
    wl_list_init(&seat->keyboards);
    wl_list_init(&seat->keyboard_groups);

    seat->cursor = wlr_cursor_create();
    if (!seat->cursor) {
        wlr_log(WLR_ERROR, "Cannot create cursor");
        wl_list_remove(&seat->listen_destroy.link);
        wl_list_remove(&seat->listen_new_input.link);

        free(seat);

        return NULL;
    }

    wlr_cursor_attach_output_layout(seat->cursor,
            seat->server->root->wlr_output_layout);

    seat->xcursor_manager = wlr_xcursor_manager_create(NULL, XCURSOR_SIZE);
    if (!seat->xcursor_manager) {
        wlr_log(WLR_ERROR, "Cannot create xcursor manager");
        wlr_cursor_destroy(seat->cursor);
        wl_list_remove(&seat->listen_destroy.link);
        wl_list_remove(&seat->listen_new_input.link);

        free(seat);

        return NULL;
    }

    wlr_xcursor_manager_load(seat->xcursor_manager, 1);

    seat->listen_request_set_cursor.notify = handle_request_set_cursor;
    wl_signal_add(&seat->wlr_seat->events.request_set_cursor, &seat->listen_request_set_cursor);
    seat->listen_cursor_motion.notify = handle_cursor_motion;
    wl_signal_add(&seat->cursor->events.motion, &seat->listen_cursor_motion);
    seat->listen_cursor_motion_absolute.notify = handle_cursor_motion_absolute;
    wl_signal_add(&seat->cursor->events.motion_absolute, &seat->listen_cursor_motion_absolute);
    seat->listen_cursor_button.notify = handle_cursor_button;
    wl_signal_add(&seat->cursor->events.button, &seat->listen_cursor_button);
    seat->listen_cursor_axis.notify = handle_cursor_axis;
    wl_signal_add(&seat->cursor->events.axis, &seat->listen_cursor_axis);
    seat->listen_cursor_frame.notify = handle_cursor_frame;
    wl_signal_add(&seat->cursor->events.frame, &seat->listen_cursor_frame);

    seat->cursor_mode = NC_CURSOR_MODE_PASSTHROUGH;

    seat->listen_request_set_selection.notify = handle_request_set_selection;
    wl_signal_add(&seat->wlr_seat->events.request_set_selection,
            &seat->listen_request_set_selection);

    return seat;
}

static void handle_new_input(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Handling new input");
    struct nc_seat *seat = wl_container_of(listener, seat, listen_new_input);
    struct wlr_input_device *device = data;

    switch (device->type) {
        case WLR_INPUT_DEVICE_KEYBOARD:
            handle_new_keyboard(seat, device);
            break;
        case WLR_INPUT_DEVICE_POINTER:
            handle_new_pointer(seat, device);
            break;
        case WLR_INPUT_DEVICE_TOUCH:
            wlr_log(WLR_DEBUG, "Touch input is not implemented");
            break;
        case WLR_INPUT_DEVICE_SWITCH:
            wlr_log(WLR_DEBUG, "Switch input not implemented");
            break;
        case WLR_INPUT_DEVICE_TABLET_TOOL:
        case WLR_INPUT_DEVICE_TABLET_PAD:
            wlr_log(WLR_DEBUG, "Table input is not implemented");
            break;
    }

    update_capabilities(seat);
}

static void handle_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Destroying seat");
    struct nc_seat *seat = wl_container_of(listener, seat, listen_destroy);

    wl_list_remove(&seat->listen_destroy.link);
    wl_list_remove(&seat->listen_new_input.link);
    wl_list_remove(&seat->listen_request_set_cursor.link);
    wl_list_remove(&seat->listen_cursor_motion.link);
    wl_list_remove(&seat->listen_cursor_motion_absolute.link);
    wl_list_remove(&seat->listen_cursor_button.link);
    wl_list_remove(&seat->listen_cursor_axis.link);
    wl_list_remove(&seat->listen_cursor_frame.link);

    struct nc_pointer *pointer;
    wl_list_for_each(pointer, &seat->pointers, link) {
        handle_pointer_destroy(&pointer->listen_destroy, NULL);
    }

    struct nc_keyboard_group *group;
    wl_list_for_each(group, &seat->keyboard_groups, link) {
        wlr_keyboard_group_destroy(group->wlr_group);
        free(group);
    }

    wlr_xcursor_manager_destroy(seat->xcursor_manager);
    if (seat->cursor) {
        wlr_cursor_destroy(seat->cursor);
    }

    free(seat);
}

static void handle_new_keyboard(struct nc_seat *seat, struct wlr_input_device *device) {
    wlr_log(WLR_INFO, "Creating new keyboard");
    struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    if (!context) {
        wlr_log(WLR_ERROR, "Unable to create XKB context");
        return;
    }

    struct xkb_rule_names rules = {0};
    rules.rules = getenv("XKB_DEFAULT_RULES");
    rules.model = getenv("XKB_DEFAULT_MODEL");
    rules.layout = getenv("XKB_DEFAULT_LAYOUT");
    rules.variant = getenv("XKB_DEFAULT_VARIANT");
    rules.options = getenv("XKB_DEFAULT_OPTIONS");
    struct xkb_keymap *keymap = xkb_map_new_from_names(context, &rules, XKB_KEYMAP_COMPILE_NO_FLAGS);
    if (!keymap) {
        wlr_log(WLR_ERROR, " Unable to configure keyboard: keymap does not exist.");
        xkb_context_unref(context);
        return;
    }

    wlr_keyboard_set_keymap(device->keyboard, keymap);

    xkb_keymap_unref(keymap);
    xkb_context_unref(context);
    wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

    keyboard_group_add(device, seat);

    wlr_seat_set_keyboard(seat->wlr_seat, device);
}

static void handle_new_pointer(struct nc_seat *seat, struct wlr_input_device *device) {
    wlr_log(WLR_INFO, "Creating new pointer");
    struct nc_pointer *pointer = calloc(1, sizeof(struct nc_pointer));
    if (!pointer) {
        wlr_log(WLR_ERROR, "Cannot allocate pointer");
        return;
    }

    pointer->seat = seat;
    pointer->device = device;
    wlr_cursor_attach_input_device(seat->cursor, device);
    if (device->output_name != NULL) {
        struct nc_output *output;
        wl_list_for_each(output, &seat->server->root->outputs, link) {
            if (strcmp(device->output_name, output->wlr_output->name) == 0) {
                wlr_cursor_map_input_to_output(seat->cursor, device, output->wlr_output);
                break;
            }
        }
    }

    wl_list_insert(&seat->pointers, &pointer->link);

    pointer->listen_destroy.notify = handle_pointer_destroy;
    wl_signal_add(&device->events.destroy, &pointer->listen_destroy);
}

// computes the capabilities of this seat
static void update_capabilities(struct nc_seat *seat) {
    uint32_t caps = 0;

    if (!wl_list_empty(&seat->pointers)) {
        caps |= WL_SEAT_CAPABILITY_POINTER;
    }
    if (!wl_list_empty(&seat->keyboard_groups)) {
        caps |= WL_SEAT_CAPABILITY_KEYBOARD;
    }

    wlr_seat_set_capabilities(seat->wlr_seat, caps);

    // Hide the cursor if seat doesn't have pointer capability
    if ((caps & WL_SEAT_CAPABILITY_POINTER) == 0) {
        wlr_cursor_set_image(seat->cursor, NULL, 0, 0, 0, 0, 0, 0);
    } else {
        wlr_xcursor_manager_set_cursor_image(seat->xcursor_manager, DEFAULT_XCURSOR, seat->cursor);
        wlr_log(WLR_INFO, "setting xcursor image");
    }
}

static void handle_pointer_destroy(struct wl_listener *listener, void *data) {
    struct nc_pointer *pointer = wl_container_of(listener, pointer, listen_destroy);
    struct nc_seat *seat = pointer->seat;

    wl_list_remove(&pointer->link);
    wlr_cursor_detach_input_device(seat->cursor, pointer->device);
    wl_list_remove(&pointer->listen_destroy.link);
    free(pointer);

    update_capabilities(seat);
}

static void handle_cursor_motion(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_cursor_motion);
    struct wlr_event_pointer_motion *event = data;

    wlr_cursor_move(seat->cursor, event->device,
            event->delta_x, event->delta_y);
    process_cursor_motion(seat, event->time_msec);
}

static void handle_cursor_motion_absolute(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_cursor_motion_absolute);
    struct wlr_event_pointer_motion_absolute *event = data;

    wlr_cursor_warp_absolute(seat->cursor, event->device, event->x, event->y);
    process_cursor_motion(seat, event->time_msec);
}

static void handle_cursor_button(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_cursor_button);
    struct wlr_event_pointer_button *event = data;

    wlr_seat_pointer_notify_button(seat->wlr_seat,
            event->time_msec, event->button, event->state);

    if (event->state == WLR_BUTTON_RELEASED) {
        seat->cursor_mode = NC_CURSOR_MODE_PASSTHROUGH;
    }
    
    double sx, sy;
    struct wlr_surface *wlr_surface;
    if (nc_surface_at(seat->server, seat->cursor->x, seat->cursor->y,
                &wlr_surface, &sx, &sy)) {
        if (wlr_surface_is_layer_surface(wlr_surface)) {
            struct wlr_layer_surface_v1 *layer_surface =
                wlr_layer_surface_v1_from_wlr_surface(wlr_surface);
            struct nc_focus_target target = {
                .type = NC_FOCUS_TARGET_LAYER,
                .layer_surface = layer_surface->data,
            };
            nc_seat_focus_target(seat, &target);
        }

        if (wlr_surface_is_xdg_surface(wlr_surface)) {
            struct wlr_xdg_surface *wlr_xdg_surface =
                wlr_xdg_surface_from_wlr_surface(wlr_surface);
            if (wlr_xdg_surface->role == WLR_XDG_SURFACE_ROLE_TOPLEVEL
                    && event->state == WLR_BUTTON_PRESSED) {
                nc_seat_focus_view(seat, wlr_xdg_surface->data);
            }
        }
    }
}

static void handle_cursor_axis(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_cursor_axis);
    struct wlr_event_pointer_axis *event = data;

    wlr_seat_pointer_notify_axis(seat->wlr_seat,
            event->time_msec, event->orientation, event->delta,
            event->delta_discrete, event->source);
}

static void handle_cursor_frame(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_cursor_frame);

    wlr_seat_pointer_notify_frame(seat->wlr_seat);
}

static void process_cursor_motion(struct nc_seat *seat, uint32_t time) {
    /*wlr_log(WLR_INFO, "x: %f y: %f", seat->cursor->x, seat->cursor->y);*/
    switch (seat->cursor_mode) {
    case NC_CURSOR_MODE_MOVE:
        process_cursor_move(seat, time);
        return;
    case NC_CURSOR_MODE_RESIZE:
        process_cursor_resize(seat, time);
        return;
    default:
        break;
    }

    double sx, sy;
    struct nc_server *server = seat->server;
    struct wlr_surface *surface = NULL;
    if (nc_surface_at(server, seat->cursor->x, seat->cursor->y,
                &surface, &sx, &sy)) {
        bool focused_changed = seat->wlr_seat->pointer_state.focused_surface != surface;
        if (focused_changed) {
            wlr_seat_pointer_notify_enter(seat->wlr_seat, surface, sx, sy);
        } else {
            wlr_seat_pointer_notify_motion(seat->wlr_seat, time, sx, sy);
        }
    } else {
        wlr_xcursor_manager_set_cursor_image(seat->xcursor_manager,
                DEFAULT_XCURSOR, seat->cursor);
        wlr_seat_pointer_clear_focus(seat->wlr_seat);
    }
}

static void process_cursor_move(struct nc_seat *seat, uint32_t time) {
    struct nc_server *server = seat->server;
    nc_move_view(server->grab_state.view,
            seat->cursor->x - server->grab_state.grab_x,
            seat->cursor->y - server->grab_state.grab_y);

    nc_transaction_clear(server->root->transaction);
    nc_view_set_maximized(server->grab_state.view, false);
    nc_transaction_start(server->root->transaction);
}

static void process_cursor_resize(struct nc_seat *seat, uint32_t time) {
    struct nc_server *server = seat->server;
    struct nc_view *view = server->grab_state.view;
    double border_x = seat->cursor->x - server->grab_state.grab_x;
    double border_y = seat->cursor->y - server->grab_state.grab_y;
    int new_left = server->grab_state.box.x;
    int new_right = server->grab_state.box.x + server->grab_state.box.width;
    int new_top = server->grab_state.box.y;
    int new_bottom = server->grab_state.box.y + server->grab_state.box.height;

    if (server->grab_state.resize_edges & WLR_EDGE_TOP) {
        new_top = border_y;
        if (new_top >= new_bottom) {
            new_top = new_bottom - 1;
        }
    } else if (server->grab_state.resize_edges & WLR_EDGE_BOTTOM) {
        new_bottom = border_y;
        if (new_bottom <= new_top) {
            new_bottom = new_top + 1;
        }
    }
    if (server->grab_state.resize_edges & WLR_EDGE_LEFT) {
        new_left = border_x;
        if (new_left >= new_right) {
            new_left = new_right - 1;
        }
    } else if (server->grab_state.resize_edges & WLR_EDGE_RIGHT) {
        new_right = border_x;
        if (new_right <= new_left) {
            new_right = new_left + 1;
        }
    }

    struct wlr_box geo_box;
    wlr_xdg_surface_get_geometry(view->xdg_surface, &geo_box);

    uint32_t new_width = new_right - new_left;
    uint32_t new_height = new_bottom - new_top;

    struct wlr_xdg_toplevel *toplevel = view->xdg_surface->toplevel;
    if (((toplevel->current.min_width == 0) || (new_width >= toplevel->current.min_width)) &&
            ((toplevel->current.max_width == 0) || (new_width <= toplevel->current.max_width)) &&
            ((toplevel->current.min_height == 0) || (new_height >= toplevel->current.min_height)) &&
            ((toplevel->current.max_height == 0) || (new_height <= toplevel->current.max_height))) {
        geo_box.x = new_left - geo_box.x;
        geo_box.y = new_top - geo_box.y;

        nc_transaction_clear(server->root->transaction);
        nc_view_set_box(view, geo_box.x, geo_box.y, new_width, new_height);
        nc_transaction_start(server->root->transaction);
    }
}

static void keyboard_group_add(struct wlr_input_device *device, struct nc_seat *seat) {
    struct wlr_keyboard *wlr_keyboard = device->keyboard;

    struct nc_keyboard_group *group;
    wl_list_for_each (group, &seat->keyboard_groups, link) {
        struct wlr_keyboard_group *wlr_group = group->wlr_group;
        if (wlr_keyboard_group_add_keyboard(wlr_group, wlr_keyboard)) {
            wlr_log(WLR_DEBUG, "Added new keyboard to existing group");
            return;
        }
    }

    // keyboard could not be inserted into any existing group
    struct nc_keyboard_group *nc_group = calloc(1, sizeof(struct nc_keyboard_group));
    if (nc_group == NULL) {
        wlr_log(WLR_ERROR, "Failed to allocate keyboard group");
        return;
    }
    nc_group->seat = seat;
    nc_group->wlr_group = wlr_keyboard_group_create();
    if (nc_group->wlr_group == NULL) {
        wlr_log(WLR_ERROR, "Failed to allocate wlr keyboard group");
        goto cleanup;
    }

    nc_group->wlr_group->data = nc_group;
    wlr_keyboard_set_keymap(&nc_group->wlr_group->keyboard, device->keyboard->keymap);

    wlr_keyboard_set_repeat_info(&nc_group->wlr_group->keyboard, wlr_keyboard->repeat_info.rate,
            wlr_keyboard->repeat_info.delay);

    wlr_log(WLR_INFO, "Created keyboard group");

    wlr_keyboard_group_add_keyboard(nc_group->wlr_group, wlr_keyboard);
    wl_list_insert(&seat->keyboard_groups, &nc_group->link);

    nc_group->listen_key.notify = handle_keyboard_group_key;
    wl_signal_add(&nc_group->wlr_group->keyboard.events.key, &nc_group->listen_key);
    nc_group->listen_modifiers.notify = handle_keyboard_group_modifiers;
    wl_signal_add(&nc_group->wlr_group->keyboard.events.modifiers, &nc_group->listen_modifiers);

    return;

cleanup:
    if (nc_group && nc_group->wlr_group) {
        wlr_keyboard_group_destroy(nc_group->wlr_group);
    }
    free(nc_group);
}

static void handle_keyboard_group_key(struct wl_listener *listener, void *data) {
    struct nc_keyboard_group *group = wl_container_of(listener, group, listen_key);
    handle_key_event(group->wlr_group->input_device, group->seat, data);
}

static void handle_keyboard_group_modifiers(struct wl_listener *listener, void *data) {
    struct nc_keyboard_group *group = wl_container_of(listener, group, listen_modifiers);
    handle_modifier_event(group->wlr_group->input_device, group->seat);
}

static void handle_key_event(struct wlr_input_device *device, struct nc_seat *seat, void *data) {
    struct wlr_event_keyboard_key *event = data;

    // translate from libinput keycode to an xkbcommon keycode.
    xkb_keycode_t keycode = event->keycode + 8;

    const xkb_keysym_t *syms;
    int nsyms = xkb_state_key_get_syms(device->keyboard->xkb_state, keycode, &syms);

    const xkb_keysym_t *raw_syms;
    const xkb_layout_index_t layout_index = xkb_state_key_get_layout(device->keyboard->xkb_state,
            keycode);
    int nraw_syms = xkb_keymap_key_get_syms_by_level(
            device->keyboard->keymap, keycode,
            layout_index, 0, &raw_syms);

    bool handled = false;

    uint32_t modifiers = wlr_keyboard_get_modifiers(device->keyboard);
    if (event->state == WLR_KEY_PRESSED) {
        for (int i = 0;i < nsyms;i++) {
            if (handle_builtin_keybind(seat, syms[i])) {
                handled = true;
                break;
            } else if (handle_keybind(seat, syms[i], modifiers)) {
                handled = true;
                break;
            }
        }

        if (!handled) {
            for (int i = 0;i < nraw_syms;i++) {
                if (handle_builtin_keybind(seat, raw_syms[i])) {
                    handled = true;
                    break;
                } else if (handle_keybind(seat, raw_syms[i], modifiers)) {
                    handled = true;
                    break;
                }
            }
        }
    }

    if (!handled) {
        wlr_seat_set_keyboard(seat->wlr_seat, device);
        wlr_seat_keyboard_notify_key(seat->wlr_seat, event->time_msec,
                event->keycode, event->state);
    }
}

static void handle_modifier_event(struct wlr_input_device *device, struct nc_seat *seat) {
    wlr_seat_set_keyboard(seat->wlr_seat, device);
    wlr_seat_keyboard_notify_modifiers(seat->wlr_seat, &device->keyboard->modifiers);
}

static void handle_request_set_cursor(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_request_set_cursor);

    // client has provided a new cursor image
    struct wlr_seat_pointer_request_set_cursor_event *event = data;
    struct wlr_seat_client *focused_client =
        seat->wlr_seat->pointer_state.focused_client;

    if (focused_client == event->seat_client) {
        wlr_cursor_set_surface(seat->cursor, event->surface,
                event->hotspot_x, event->hotspot_y);
    }
}

static bool handle_keybind(struct nc_seat *seat, xkb_keysym_t keysym, uint32_t modifiers) {
    switch (keysym) {
    case XKB_KEY_Escape:
        wl_display_terminate(seat->server->wl_display);
        return true;
    case XKB_KEY_F1:
        if (fork() == 0) {
            execl("/bin/sh", "/bin/sh", "-c", getenv("TERMINAL"), (char *)NULL);
        }
        return true;
    default:
        return false;
    }
}

static bool handle_builtin_keybind(struct nc_seat *seat, xkb_keysym_t keysym) {
    if (keysym >= XKB_KEY_XF86Switch_VT_1 && keysym <= XKB_KEY_XF86Switch_VT_12) {
        wlr_log(WLR_INFO, "Switch VT keysym received");
        struct wlr_backend *backend = seat->server->backend;
        if (wlr_backend_is_multi(backend)) {
            struct wlr_session *session = wlr_backend_get_session(backend);
            if (backend) {
                unsigned vt = keysym - XKB_KEY_XF86Switch_VT_1 + 1;
                wlr_log(WLR_INFO, "Switching to VT: %d", vt);
                wlr_session_change_vt(session, vt);
            }
        }

        return true;
    }

    return false;
}

static void handle_request_set_selection(struct wl_listener *listener, void *data) {
    struct nc_seat *seat = wl_container_of(listener, seat, listen_request_set_selection);
    struct wlr_seat_request_set_selection_event *event = data;

    wlr_seat_set_selection(seat->wlr_seat, event->source, event->serial);
}

void nc_seat_focus_view(struct nc_seat *seat, struct nc_view *view) {
    if (view == NULL) {
        // choose the topmost visible view
        struct nc_view *_view;
        wl_list_for_each(_view, &seat->server->root->views, link) {
            if (_view->mapped) {
                view = _view;
                break;
            }
        }
    }
    if (view != NULL) {
        // move the view to the top
        wl_list_remove(&view->link);
        wl_list_insert(&seat->server->root->views, &view->link);
    }

    struct nc_focus_target target;
    if (view != NULL) {
        target.type = NC_FOCUS_TARGET_VIEW;
        target.view = view;
    } else {
        target.type = NC_FOCUS_TARGET_NONE;
    }

    nc_seat_focus_target(seat, &target);
}

void nc_seat_focus_target(struct nc_seat *seat, struct nc_focus_target *target) {
    switch (target->type) {
    case NC_FOCUS_TARGET_VIEW:
        if (target->view == seat->focused_view) {
            return;
        }
        break;
    case NC_FOCUS_TARGET_LAYER:
        if (target->layer_surface == seat->focused_layer) {
            return;
        }
        break;
    default:
        break;
    }

    struct wlr_surface *target_surface;
    switch (target->type) {
    case NC_FOCUS_TARGET_NONE:
        target_surface = NULL;
        break;
    case NC_FOCUS_TARGET_VIEW:
        target_surface = target->view->xdg_surface->surface;
        break;
    case NC_FOCUS_TARGET_LAYER:
        target_surface = target->layer_surface->wlr_layer_surface->surface;
        break;
    default:
        target_surface = NULL;
        break;
    }

    if (seat->focused_view != NULL) {
        nc_view_set_focused(seat->focused_view, false);
        seat->focused_view = NULL;
    }
    if (seat->focused_layer != NULL) {
        seat->focused_view = NULL;
    }
    
    wlr_seat_keyboard_clear_focus(seat->wlr_seat);

    switch (target->type) {
    case NC_FOCUS_TARGET_NONE:
        break;
    case NC_FOCUS_TARGET_VIEW:
        nc_view_set_focused(target->view, true);
        seat->focused_view = target->view;
        break;
    case NC_FOCUS_TARGET_LAYER:
        seat->focused_layer = target->layer_surface;
        break;
    default:
        break;
    }

    if (target_surface != NULL) {
        struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(seat->wlr_seat);
        if (keyboard != NULL) {
            wlr_seat_keyboard_notify_enter(seat->wlr_seat,
                    target_surface, keyboard->keycodes, keyboard->num_keycodes,
                    &keyboard->modifiers);
        } else {
            wlr_seat_keyboard_notify_enter(seat->wlr_seat,
                    target_surface, NULL, 0, NULL);
        }
    }
}

void nc_seat_handle_view_unmap(struct nc_seat *seat, struct nc_view *view) {
    if (seat->focused_view == view) {
        nc_seat_focus_view(seat, NULL);
    }
}

void nc_seat_handle_layer_unmap(struct nc_seat *seat, struct nc_layer_surface *layer_surface) {
    if (seat->focused_layer == layer_surface) {
        // TODO check if there is another layer surface

        nc_seat_focus_view(seat, NULL);
    }
}
