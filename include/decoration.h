#ifndef _NC_DECORATION_H
#define _NC_DECORATION_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>

struct nc_decoration_manager {
    struct nc_server *server;
    struct wlr_xdg_decoration_manager_v1 *wlr_xdg_decoration_manager;

    struct wl_list decorations; // nc_decoration::link

    struct wl_listener listen_new_toplevel_decoration;
};

struct nc_decoration {
    struct nc_decoration_manager *decoration_manager;
    struct wlr_xdg_toplevel_decoration_v1 *wlr_xdg_toplevel_decoration;

    struct wl_listener listen_request_mode;

    struct wl_list link; // nc_decoration_manager::decorations
};

struct nc_decoration_manager *nc_decoration_manager_create(struct nc_server *server);
#endif
