#ifndef _NC_RENDER_H
#define _NC_RENDER_H

#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output.h>

struct nc_output;
struct nc_view;

struct nc_surface_render_data {
    struct nc_output *output;
    struct wlr_renderer *renderer;
    //struct nc_view *view;
    int32_t output_x;
    int32_t output_y;
    struct timespec *when;
    bool output_local;
};

void nc_render_frame(struct nc_output *output);

#endif
