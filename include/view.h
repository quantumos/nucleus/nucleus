#ifndef _NC_VIEW_H
#define _NC_VIEW_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_shell.h>

struct nc_view {
    struct nc_server *server;
    struct wlr_xdg_surface *xdg_surface;

    struct wl_listener listen_map;
    struct wl_listener listen_unmap;
    struct wl_listener listen_move;
    struct wl_listener listen_resize;
    struct wl_listener listen_maximize;
    struct wl_listener listen_minimize;
    struct wl_listener listen_commit;
    struct wl_listener listen_destroy;

    bool mapped;

    struct wlr_box current_box;
    uint32_t tiled_edges;
    bool maximized;
    struct wlr_box untiled_box;

    struct wlr_client_buffer *stashed_buffer;
    uint32_t stashed_width, stashed_height;

    struct wl_list link; // nc_server::views
};

struct nc_view_grab_state {
    struct nc_view *view;
    double grab_x, grab_y;
    struct wlr_box box;
    uint32_t resize_edges;
};

void handle_new_xdg_surface(struct wl_listener *listener, void *data);
void nc_move_view(struct nc_view *view, double x, double y);
void nc_view_stash_buffer(struct nc_view *view);
void nc_view_unstash_buffer(struct nc_view *view);
bool nc_view_needs_configure(struct nc_view *view);
void nc_view_configure(struct nc_view *view);
void nc_view_send_frame_done(struct nc_view *view);
void nc_view_set_focused(struct nc_view *view, bool focused);
struct nc_transaction_instruction *nc_view_set_box(struct nc_view *view,
        int x, int y, int width, int height);
struct nc_transaction_instruction *nc_view_set_tiled(struct nc_view *view,
        uint32_t tiled_edges);
struct nc_transaction_instruction *nc_view_set_maximized(struct nc_view *view,
        bool maximized);
void nc_view_minimize(struct nc_view *view);
void nc_view_unminimize(struct nc_view *view);

#endif
