#ifndef _NC_SURFACE_H
#define _NC_SURFACE_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_surface.h>

#include "server.h"

bool nc_surface_at(struct nc_server *server,
        double lx, double ly, struct wlr_surface **surface,
        double *sx, double *sy);
void nc_surface_send_frame_done(struct wlr_surface *surface);

#endif
