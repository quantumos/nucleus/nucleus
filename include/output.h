#ifndef _NC_OUTPUT_H
#define _NC_OUTPUT_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_output.h>

struct nc_server;

struct nc_output {
    struct wlr_output *wlr_output; // wlroots output this struct is associated with
    struct nc_server *server; // nucleus server this struct is associated with
    struct timespec last_frame; // time when last frame was rendered to this output

    struct wl_listener listen_frame; // listener for when this output requests a new frame
    struct wl_listener listen_destroy; // listener for when this output is destroyed

    struct wl_list layers[4]; // nc_layer_surface::link

    struct wl_list link; // nc_server::outputs
};

void handle_new_output(struct wl_listener *listener, void *data);
struct nc_output *nc_output_create(struct wlr_output *wlr_output);
void nc_output_destroy(struct nc_output *output);

#endif
