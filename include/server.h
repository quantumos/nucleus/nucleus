#ifndef _NC_SERVER_H
#define _NC_SERVER_H

#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output_layout.h>
#include "wlr-layer-shell-unstable-v1-protocol.h"

#include "seat.h"
#include "view.h"
#include "root.h"
#include "decoration.h"

struct nc_server {
    struct wl_display *wl_display;
    struct wl_event_loop *wl_event_loop;
    struct wlr_backend *backend;
    struct wlr_backend *noop_backend;
    struct wlr_renderer *renderer;
    const char *socket;

    struct nc_decoration_manager *decoration_manager;
    struct nc_root *root;
    struct nc_seat *seat;
    struct wlr_xdg_shell *xdg_shell;
    struct wlr_layer_shell_v1 *layer_shell;

    struct wl_listener listen_new_output;
    struct wl_listener listen_new_xdg_surface;
    struct wl_listener listen_new_layer_surface;
    
    struct nc_view_grab_state grab_state;
};

bool nc_server_init(struct nc_server *server);
bool nc_server_start(struct nc_server *server);
void nc_server_run(struct nc_server *server);
void nc_server_destroy(struct nc_server *server);

#endif
