#ifndef _NC_SEAT_H
#define _NC_SEAT_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_seat.h>

#include "server.h"

enum nc_cursor_mode {
    NC_CURSOR_MODE_PASSTHROUGH,
    NC_CURSOR_MODE_MOVE,
    NC_CURSOR_MODE_RESIZE,
};

struct nc_seat {
    struct wlr_seat *wlr_seat;
    struct nc_server *server;

    struct wl_listener listen_new_input;
    struct wl_listener listen_destroy;
    
    struct wl_list pointers;
    struct wl_list keyboards;
    struct wl_list keyboard_groups;

    struct wlr_cursor *cursor;
    struct wlr_xcursor_manager *xcursor_manager;
    enum nc_cursor_mode cursor_mode;
    struct wl_listener listen_request_set_cursor;
    struct wl_listener listen_cursor_motion;
    struct wl_listener listen_cursor_motion_absolute;
    struct wl_listener listen_cursor_button;
    struct wl_listener listen_cursor_axis;
    struct wl_listener listen_cursor_frame;

    struct wl_listener listen_request_set_selection;

    struct nc_view *focused_view;
    struct nc_layer_surface *focused_layer;
};

struct nc_pointer {
    struct nc_seat *seat;
    struct wlr_input_device *device;

    struct wl_listener listen_destroy;

    struct wl_list link; // seat::pointers
};

struct nc_keyboard_group {
    struct wlr_keyboard_group *wlr_group;
    struct nc_seat *seat;
    struct wl_listener listen_key;
    struct wl_listener listen_modifiers;
    struct wl_list link; // nc_seat::keyboard_groups
};

enum nc_focus_target_type {
    NC_FOCUS_TARGET_NONE,
    NC_FOCUS_TARGET_VIEW,
    NC_FOCUS_TARGET_LAYER,
};

struct nc_focus_target {
    enum nc_focus_target_type type;
    union {
        struct nc_view *view;
        struct nc_layer_surface *layer_surface;
    };
};

struct nc_seat *nc_seat_create(struct nc_server *server);
void nc_seat_focus_view(struct nc_seat *seat, struct nc_view *view);
void nc_seat_focus_target(struct nc_seat *seat, struct nc_focus_target *target);
void nc_seat_handle_view_unmap(struct nc_seat *seat, struct nc_view *view);
void nc_seat_handle_layer_unmap(struct nc_seat *seat, struct nc_layer_surface *layer_surface);

#endif
