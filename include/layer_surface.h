#ifndef _NC_LAYER_SURFACE_H
#define _NC_LAYER_SURFACE_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_layer_shell_v1.h>

#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "output.h"

struct nc_layer_surface {
    struct nc_output *output;
    struct wlr_box box;
    struct wlr_layer_surface_v1 *wlr_layer_surface;
    enum zwlr_layer_shell_v1_layer layer;

    struct wl_listener listen_map;
    struct wl_listener listen_unmap;
    struct wl_listener listen_commit;
    struct wl_listener listen_destroy;

    struct wl_list link; // nc_output::layers[layer_idx]
};

void handle_new_layer_surface(struct wl_listener *listener, void *data);

#endif
