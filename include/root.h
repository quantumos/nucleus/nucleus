#ifndef _NC_ROOT_H
#define _NC_ROOT_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_output_layout.h>

struct nc_root {
    struct nc_server *server;

    struct wlr_output_layout *wlr_output_layout;
    struct wl_list outputs; // nc_output::link
    struct nc_output *noop_output;

    struct wl_list views; // nc_view::link

    // currently we only have a single transaction at a time,
    // in the future we might want/have to queue multiple transactions
    struct nc_transaction *transaction;
};

struct nc_root *nc_root_create(struct nc_server *server);
void nc_root_destroy(struct nc_root *root);

#endif
