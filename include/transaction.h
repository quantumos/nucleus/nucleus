#ifndef _NC_TRANSACTION_H
#define _NC_TRANSACTION_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_box.h>

struct nc_transaction {
    struct nc_root *root;
    struct wl_event_source *timer;
    struct wl_list instructions; // nc_transaction_instruction::link
    size_t num_pending;
};

enum nc_transaction_instruction_type {
    NC_TRA_INS_SET_BOX,
    NC_TRA_INS_SET_TILED,
    NC_TRA_INS_SET_MAXIMIZED,
};

struct nc_transaction_instruction {
    struct nc_view *target;
    enum nc_transaction_instruction_type type;

    union {
        struct wlr_box set_box_data;
        uint32_t set_tiled_data;
        bool set_maximized_data;
    };

    uint32_t serial;

    struct wl_list link;
};

struct nc_transaction *nc_transaction_create(struct nc_root *root);
void nc_transaction_add_instruction(struct nc_transaction *transaction,
        struct nc_transaction_instruction *instruction);
void nc_transaction_clear(struct nc_transaction *transaction);
void nc_transaction_start(struct nc_transaction *transaction);
void nc_transaction_commit(struct nc_transaction *transaction);
void nc_transaction_notify(struct nc_transaction *transaction);

#endif
