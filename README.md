# Nucleus
Welcome to the Nucleus compositor, the core of the Nucleus Desktop Environment project. We aim to create a modern desktop environment which is easy to understand and fun to use, powerful and stunning to look at, and respects computer resources. Nucleus is built for Wayland; an X11 port is not planned. Other repositories contain the other parts of the compositor, including desktop widgets such as the dock and launcher.

## Warning
Nucleus is currently still under early development. While you are welcome to try it out, it is still far from meeting its goals, and will probably be buggy. Consider contributing, either in code or helping with the design of Nucleus!

## Usability
Anyone who has used a computer before should be able to pick up Nucleus without much problem and start using it. If you have considerable difficulty understanding how to use Nucleus, we consider that an issue, please file a report. At the same time, it should be feature-rich enough that more advanced users do not feel held back.

## Technical Design
The compositor uses the [wlroots](https://github.com/swaywm/wlroots) library at its core. It is written in C (not C++). Other desktop components are (for the most part) written in Rust. Nucleus puts a heavy importance on efficiency and resource consumption, so more resource intensive paradigms such as an embedded Javascript runtime, etc will not be considered. Currently it uses wlroots' native renderer; in the future, a powerful GLES/Vulkan-hybrid renderer will be developed to fit more complex requirements.

TODO: much more to be written
